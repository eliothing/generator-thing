"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

describe("generator-thing:repo", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/repo"))
      .inDir(
        path.join(
          __dirname,
          "test_generated",
          "repo_test",
          "evilRepoWizard",
          "brave-repo-apprentice",
        ),
      )
      .withOptions({})
      .on("end", done)
  })

  it("creates files", () => {
    assert.file(".gitignore")
    assert.file(".npmignore")
    assert.file(".prettierignore")
    assert.file(".prettierrc")
    assert.file("LICENSE")
    assert.file("package.json")
    assert.file("README.md")
  })

  it("properly inits package.json", () => {
    assert.fileContent("package.json", `"name": "brave-repo-apprentice"`)
    assert.fileContent("package.json", `"identifier": "brave-repo-apprentice"`)
    assert.fileContent("package.json", `"subjectOf": "evilRepoWizard"`)
  })

  it("standard README.md", () => {
    assert.fileContent(
      "README.md",
      "![](https://elioway.gitlab.io/evilRepoWizard/brave-repo-apprentice/elio-brave-repo-apprentice-logo.png)",
    )
    assert.fileContent(
      "README.md",
      "- [brave-repo-apprentice Documentation](https://elioway.gitlab.io/evilRepoWizard/brave-repo-apprentice/)",
    )
    assert.fileContent(
      "README.md",
      "- [evilRepoWizard Quickstart](https://elioway.gitlab.io/evilRepoWizard/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [brave-repo-apprentice Quickstart](https://elioway.gitlab.io/evilRepoWizard/brave-repo-apprentice/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [brave-repo-apprentice Credits](https://elioway.gitlab.io/evilRepoWizard/brave-repo-apprentice/credits.html)",
    )
    assert.fileContent(
      "README.md",
      "- [Installing brave-repo-apprentice](https://elioway.gitlab.io/evilRepoWizard/brave-repo-apprentice/installing.html)",
    )
  })
})
