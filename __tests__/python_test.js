"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

var deps = [[helpers.createDummyGenerator(), "thing:repo"]]

describe("generator-thing:python", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/python"))
      .inDir(path.join(__dirname, "test_generated", "python_test"))
      .withGenerators(deps)
      .withOptions({
        identifier: "PythonName",
        subjectOf: "eliomaster",
        skipinstall: true,
      })
      .on("end", done)
  })

  it("creates files", () => {
    assert.file("python_name/__init__.py")
    assert.file("python_name/python_name.py")
    assert.file("python_name/tests/test_python_name.py")
    assert.file("requirements/base.txt")
    assert.file("requirements/local.txt")
    assert.file(".env")
    assert.file(".env.fish")
    assert.file("python_name.py")
    assert.file("pyproject.toml")
    assert.file("README.md")
    assert.file("setup.cfg")
    assert.file("setup.py")
  })

  it("puts the identifier and subjectOf into files", () => {
    ;[
      "from python_name.python_name import python_name",
      "python_name(arg1, arg2, arg3)",
    ].forEach((s) => assert.fileContent("python_name.py", s))
    assert.fileContent("python_name/python_name.py", "def python_name():")
    ;[
      "class PythonNameTestCase",
      "def test_python_name",
      'self.assertEqual(python_name(), "pythonName")',
    ].forEach((s) =>
      assert.fileContent("python_name/tests/test_python_name.py", s),
    )
    assert.fileContent(".env", "source venv-python_name/bin/activate")
    assert.fileContent(".env.fish", "source venv-python_name/bin/activate.fish")
    assert.fileContent("README.md", "pip install python-name")
    assert.fileContent("setup.py", 'name="elio-python-name"')
    assert.fileContent("setup.py", '"elio-python-name=elio_python_name:main"')
  })

  it("standard README.md", () => {
    assert.fileContent(
      "README.md",
      "![](https://elioway.gitlab.io/eliomaster/PythonName/elio-python-name-logo.png)",
    )
    assert.fileContent(
      "README.md",
      "- [PythonName Documentation](https://elioway.gitlab.io/eliomaster/PythonName/)",
    )
    assert.fileContent(
      "README.md",
      "- [eliomaster Quickstart](https://elioway.gitlab.io/eliomaster/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [PythonName Quickstart](https://elioway.gitlab.io/eliomaster/PythonName/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [PythonName Credits](https://elioway.gitlab.io/eliomaster/PythonName/credits.html)",
    )
    assert.fileContent(
      "README.md",
      "- [Installing PythonName](https://elioway.gitlab.io/eliomaster/PythonName/installing.html)",
    )
  })
})
