"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

var deps = [[helpers.createDummyGenerator(), "thing:repo"]]

describe("generator-thing:node", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/node"))
      .inDir(path.join(__dirname, "test_generated", "node_test"))
      .withGenerators(deps)
      .withOptions({
        identifier: "NodeName",
        subjectOf: "eliomaster",
        skipinstall: true,
      })
      .on("end", done)
  })

  it("creates files", () => {
    assert.file("node-name.js")
    assert.file("package.json")
    assert.file("bin/index.js")
    assert.file("test/node-name-test.mjs")
    assert.file("test/always-passes-test.mjs")
    assert.file("test/freeze-date-test.mjs")
  })

  it("puts the identifier and subjectOf into files", () => {
    assert.fileContent("node-name.js", "module.exports = function nodeName()")
    ;[
      `"name": "@elioway/node-name"`,
      `"bin": "./bin/index.js"`,
      `"main": "./node-name.js"`,
    ].forEach((s) => assert.fileContent("package.json", s))
    assert.fileContent(
      "bin/index.js",
      `const nodeName = require("../node-name")`,
    )
  })
})
