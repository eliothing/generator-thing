"use strict"
const path = require("path")
const helpers = require("yeoman-test")
const assert = require("yeoman-assert")

var deps = [[helpers.createDummyGenerator(), "thing:repo"]]

describe("generator-thing:thing", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/thing"))
      .inDir(path.join(__dirname, "test_generated", "thing_test", "thing_test"))
      .withGenerators(deps)
      .withOptions({ identifier: "thing", subjectOf: "eliomaster" })
      .withPrompts({
        thingType: "Thing",
        scheme: false,
      })
      .on("end", done)
  })

  it("shits out a thing thinglet", () => {
    assert.file("thing.json")
    assert.noFile("Schema/thing.json")
  })
})

describe("generator-thing:thing scheme", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/thing"))
      .inDir(
        path.join(__dirname, "test_generated", "thing_test", "scheme_test"),
      )
      .withGenerators(deps)
      .withOptions({ identifier: "thing", subjectOf: "eliomaster" })
      .withPrompts({
        thingType: "Thing",
        scheme: true,
      })
      .on("end", done)
  })

  it("shits out a thing Schema", () => {
    assert.file("thing.json")
    assert.file("Schema/Thing.json")
  })
})

describe("generator-thing:thing Action", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/thing"))
      .inDir(
        path.join(__dirname, "test_generated", "thing_test", "action_test"),
      )
      .withGenerators(deps)
      .withOptions({ identifier: "action", subjectOf: "eliomaster" })
      .withPrompts({
        thingType: "Action",
        scheme: false,
      })
      .on("end", done)
  })

  it("shits out an action thinglet", () => {
    assert.file("thing.json")
    assert.noFile("Schema/thing.json")
  })
})

describe("generator-thing:thing BookmarkAction scheme", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/thing"))
      .inDir(
        path.join(
          __dirname,
          "test_generated",
          "thing_test",
          "bookmarkaction_test",
        ),
      )
      .withGenerators(deps)
      .withOptions({ identifier: "action", subjectOf: "eliomaster" })
      .withPrompts({
        thingType: "BookmarkAction",
        scheme: true,
      })
      .on("end", done)
  })

  it("shits out an bookmarkaction thinglet", () => {
    assert.file("thing.json")
    assert.file("Schema/Thing.json")
    assert.noFile("Schema/Action.json")
    assert.noFile("Schema/Action/OrganizeAction.json")
    assert.noFile("Schema/Action/OrganizeAction/BookmarkAction.json")
  })
})

describe("generator-thing:thing Hospital", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/thing"))
      .inDir(
        path.join(__dirname, "test_generated", "thing_test", "hospital_test"),
      )
      .withGenerators(deps)
      .withOptions({ identifier: "hospital", subjectOf: "eliomaster" })
      .withPrompts({
        thingType: "Hospital",
        scheme: false,
      })
      .on("end", done)
  })

  it("shits out an hospital thinglet", () => {
    assert.file("thing.json")
  })
})
