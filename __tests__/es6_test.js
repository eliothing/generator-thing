"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

var deps = [
  [helpers.createDummyGenerator(), "thing:repo"],
  [helpers.createDummyGenerator(), "thing:node"],
]

describe("generator-thing:es6", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/es6"))
      .inDir(path.join(__dirname, "test_generated", "es6_test"))
      .withGenerators(deps)
      .withOptions({
        identifier: "es6Name",
        subjectOf: "eliomaster",
        skipinstall: true,
      })
      .on("end", done)
  })

  it("creates files", () => {
    assert.file("es-6-name.js")
    assert.file("package.json")
    assert.file("bin/index.js")
    assert.file("test/es-6-name-test.mjs")
    assert.file("test/always-passes-test.mjs")
    assert.file("test/freeze-date-test.mjs")
  })

  it("puts the identifier and subjectOf into files", () => {
    assert.fileContent("es-6-name.js", "export default es6Name")
    ;[
      `"name": "@elioway/es-6-name"`,
      `"bin": "./bin/index.js"`,
      `"main": "./es-6-name.js"`,
    ].forEach((s) => assert.fileContent("package.json", s))
    assert.fileContent("bin/index.js", `import es6Name from "../es-6-name.js"`)
  })
})
