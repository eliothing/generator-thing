"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

var deps = [
  [helpers.createDummyGenerator(), "thing:repo"],
  [helpers.createDummyGenerator(), "thing:python"],
]
describe("generator-thing:flask", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/flask"))
      .inDir(path.join(__dirname, "test_generated", "flask_test"))
      .withGenerators(deps)
      .withOptions({
        identifier: "FlaskName",
        subjectOf: "eliomaster",
        skipinstall: true,
      })
      .on("end", done)
  })

  it("creates files", () => {
    assert.file("README.md")
    assert.file("run.py")
    assert.file("setup.py")
    assert.file("wsgi.py")

    assert.file("doc/installing.md")

    assert.file("installers/flask_name")
    assert.file("installers/flask_name.service")

    assert.file("flask_name/app.py")
    assert.file("flask_name/config.py")
    assert.file("flask_name/jinja_filters.py")
    assert.file("flask_name/utils.py")
    assert.file("flask_name/views.py")

    assert.file("flask_name/static/manifest.json")
    assert.file("flask_name/static/css/flask_name.min.css")
    assert.file("flask_name/static/css/flask_name.min.css.map")

    assert.file("flask_name/templates/base.html")
    assert.file("flask_name/templates/index.html")
    assert.file("flask_name/templates/flask_name.html")
  })

  it("puts the identifier and subjectOf into files", () => {
    ;[
      "virtualenv --python=python3 venv-flask_name",
      "./run.py",
      "- USER: flask_name",
    ].forEach((s) => assert.fileContent("doc/installing.md", s))
    ;[
      `from flask_name.jinja_filters import qs_active, qs_toggler`,
      `from flask_name.views import Main, FlaskName`,
      `"development": "flask_name.config.DevelopmentConfig",`,
      `"testing": "flask_name.config.TestingConfig",`,
      `"default": "flask_name.config.DevelopmentConfig",`,
      `"./flask_name/flask_name.cfg"`,
      `"flask_name", view_func=FlaskName.as_view(bucket), methods=["GET"]`,
    ].forEach((s) => assert.fileContent("flask_name/app.py", s))
    ;[`FLASKNAME_USER = "flask_name"`, `FLASKNAME_PASS = "letmein"`].forEach(
      (s) => assert.fileContent("flask_name/config.py", s),
    )
    ;[
      "class FlaskName(flask.views.MethodView):",
      `"flask_name.html",`,
      "FLASKNAME_USER",
    ].forEach((s) => assert.fileContent("flask_name/views.py", s))
    ;[
      "virtualenv --python=python3 venv-flask_name",
      "./run.py",
      "- USER: flask_name",
    ].forEach((s) => assert.fileContent("README.md", s))
    assert.fileContent("run.py", "from flask_name.app import create_app")
    assert.fileContent("wsgi.py", "from flask_name.app import create_app")
  })

  it("standard README.md", () => {
    assert.fileContent(
      "README.md",
      "![](https://elioway.gitlab.io/eliomaster/FlaskName/elio-flask-name-logo.png)",
    )
    assert.fileContent(
      "README.md",
      "- [FlaskName Documentation](https://elioway.gitlab.io/eliomaster/FlaskName/)",
    )
    assert.fileContent(
      "README.md",
      "- [eliomaster Quickstart](https://elioway.gitlab.io/eliomaster/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [FlaskName Quickstart](https://elioway.gitlab.io/eliomaster/FlaskName/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [FlaskName Credits](https://elioway.gitlab.io/eliomaster/FlaskName/credits.html)",
    )
  })
})
