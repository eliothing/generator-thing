"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

var deps = [
  [helpers.createDummyGenerator(), "thing:repo"],
  [helpers.createDummyGenerator(), "thing:python"],
]

describe("generator-thing:django", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/django"))
      .inDir(path.join(__dirname, "test_generated", "django_test"))
      .withGenerators(deps)
      .withOptions({
        identifier: "DjangoName",
        subjectOf: "eliomaster",
        skipinstall: true,
      })
      .on("end", done)
  })

  it("creates files", () => {
    assert.file("manage.py")
    assert.file("README.md")
    assert.file("run_django_name.sh")
    assert.file("setup.py")
    assert.file("django_name/models.py")
    assert.file("django_name/settings.py")
    assert.file("django_name/urls.py")
    assert.file("django_name/views.py")
    assert.file("django_name/wsgi.py")

    assert.file("django_name/static/manifest.json")
    assert.file("django_name/static/css/django_name.min.css")
    assert.file("django_name/static/css/django_name.min.css.map")
    assert.file("django_name/static/js/jquery-3.4.1.min.js")

    assert.file("django_name/templates/django_name/base.html")
    assert.file("django_name/templates/django_name/home_page.html")
    assert.file("django_name/templates/django_name/thing_delete.html")
    assert.file("django_name/templates/django_name/thing_engaged.html")
    assert.file("django_name/templates/django_name/thing_form.html")
    assert.file("django_name/templates/django_name/thing_many.html")
    assert.file("django_name/templates/django_name/thing_listed.html")
    assert.file("django_name/templates/django_name/thing_optimize.html")
    assert.file("django_name/templates/django_name/thing_page.html")

    assert.file("requirements/base.txt")
    assert.file("requirements/local.txt")
    assert.file("schemaorg/data/releases/3.9/all-layers.jsonld")
  })

  it("puts the identifier and subjectOf into files", () => {
    ;[
      "DjangoNameCreateView",
      "DjangoNameUpdateView",
      "DjangoNameDeleteView",
      "DjangoNameEngageView",
      "DjangoNameListView",
      "DjangoNameManyView",
      "DjangoNameOptimizeView",
      "DjangoNamePageView",
      "DjangoNameHomeView",
    ].forEach((s) => assert.fileContent("django_name/views.py", s))

    assert.fileContent("django_name/settings.py", '"django_name"')
    ;[
      "virtualenv --python=python3 venv-django_name",
      "django-admin genome Thing",
    ].forEach((s) => assert.fileContent("README.md", s))
  })

  it("standard README.md", () => {
    assert.fileContent(
      "README.md",
      "![](https://elioway.gitlab.io/eliomaster/DjangoName/elio-django-name-logo.png)",
    )
    assert.fileContent(
      "README.md",
      "- [eliomaster Quickstart](https://elioway.gitlab.io/eliomaster/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [DjangoName Quickstart](https://elioway.gitlab.io/eliomaster/DjangoName/quickstart.html)",
    )
    assert.fileContent(
      "README.md",
      "- [DjangoName Credits](https://elioway.gitlab.io/eliomaster/DjangoName/credits.html)",
    )
  })
})
