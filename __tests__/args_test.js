"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

describe("generator-thing args", () => {
  it("works with no args", (done) => {
    helpers
      .run(path.join(__dirname, "../generators/repo"))
      .inDir(
        path.join(
          __dirname,
          "test_generated",
          "args_test",
          "noArgs",
          "evilArgsWizard",
          "brave-args-apprentice",
        ),
      )
      .withOptions({})
      .on("end", () => {
        assert.fileContent(
          "README.md",
          "https://elioway.gitlab.io/evilArgsWizard/brave-args-apprentice/installing.html",
        )
        assert.fileContent("README.md", "# brave-args-apprentice")
        done()
      })
  })

  it("works with identifier arg", (done) => {
    helpers
      .run(path.join(__dirname, "../generators/repo"))
      .inDir(
        path.join(
          __dirname,
          "test_generated",
          "args_test",
          "identifier_arg",
          "evilArgsWizard",
        ),
      )
      .withOptions({
        identifier: "brave-args-apprentice",
      })
      .on("end", () => {
        assert.fileContent(
          "README.md",
          "https://elioway.gitlab.io/evilArgsWizard/brave-args-apprentice/installing.html",
        )
        assert.fileContent("README.md", "# brave-args-apprentice")
        done()
      })
  })

  it("works with subjectOf arg", (done) => {
    helpers
      .run(path.join(__dirname, "../generators/repo"))
      .inDir(
        path.join(
          __dirname,
          "test_generated",
          "args_test",
          "subjectOf_arg",
          "chiefWizard",
          "brave-args-apprentice",
        ),
      )
      .withOptions({
        subjectOf: "evilArgsWizard",
      })
      .on("end", () => {
        assert.fileContent(
          "README.md",
          "https://elioway.gitlab.io/evilArgsWizard/brave-args-apprentice/installing.html",
        )
        assert.fileContent("README.md", "# brave-args-apprentice")
        done()
      })
  })

  it("works with all args", (done) => {
    helpers
      .run(path.join(__dirname, "../generators/repo"))
      .inDir(
        path.join(
          __dirname,
          "test_generated",
          "args_test",
          "all_args",
          "chiefWizard",
        ),
      )
      .withOptions({
        identifier: "apprentice",
        subjectOf: "evilArgsWizard",
      })
      .on("end", () => {
        assert.fileContent(
          "README.md",
          "https://elioway.gitlab.io/evilArgsWizard/apprentice/installing.html",
        )
        assert.fileContent("README.md", "# apprentice")
        done()
      })
  })
})
