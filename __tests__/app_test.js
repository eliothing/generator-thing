"use strict"
const path = require("path")
const helpers = require("yeoman-test")
const assert = require("yeoman-assert")

var deps = [[helpers.createDummyGenerator(), "thing:repo"]]

describe("generator-thing:app", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/app"))
      .inDir(path.join(__dirname, "test_generated", "app_test"))
      .withGenerators(deps)
      .withOptions({ identifier: "AppName", subjectOf: "eliomaster" })
      .on("end", done)
  })

  it("calls repo ", () => {
    assert.file("README.md")
  })
})
