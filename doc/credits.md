# generator-thing Credits

## Core Thanks!

- [yeoman](http://yeoman.io/)

## Artwork

- [Frankenstein](https://commons.wikimedia.org/wiki/File:Frankenstein_%284888039856%29.jpg)
- [Frankenstein1931CliveKarloff](https://commons.wikimedia.org/wiki/File:Frankenstein1931CliveKarloffCrop.jpg)

## Explore

- <https://github.com/patorjk/figlet.js/tree/master/fonts>
