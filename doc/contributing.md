# Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliothing.git
cd eliothing
git clone https://gitlab.com/eliothing/generator-thing.git
cd generator-thing
```

## Testing in development

```shell
cd ~/repo/elioway/eliothing/generator-thing
npm|yarn link
cd ~/repo/newtheme
npm i|yarn add generator-thing
npm|yarn link generator-thing
yo thing:django
```

## Testing after development

```shell
cd ~/repo/elioway/eliothing/generator-thing
npm|yarn unlink
cd ~/repo/newtheme
npm i|yarn add generator-thing
```

## TODOS

1. TODOS
