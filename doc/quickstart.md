# Quickstart generator-thing

- [generator-thing Prerequisites](/eliothing/generator-thing/prerequisites.html)
- [Installing generator-thing](/eliothing/generator-thing/installing.html)

## Nutshell

- Create folder for your app.
- Enter the command `yo thing:django` in the folder.

## Exercise 1: Calling `yo thing:django`

Start by creating a folder for your app, and typing `yo thing:django`. We will use "Frankenstein" for this Quick Start.

```shell
mkdir frankenstein
cd frankenstein
yo thing:django
```

You will only be asked to provide a name of your website. It should be short but expressive: e.g. "The Kitchen", "Brazil Travels". Use "Frankenstein" to follow this Quick Start exactly, but it's not compulsory.

- Now wait...

- Files are created.

- The finished message appears.

### What `yo thing:django` makes

Assuming you ran **generator-thing** in a folder call `app_name`:

```asciiart
|- .gitignore
|- init_app_name.sh
|- LICENSE.sh
|- manage.py
|- pyproject.toml
|- README.md
|- setup.py
|- app_name
    |- __init__.py
    |- models.py
    |- settings.py
    |- urls.py
    |- views.py
    |- wsgi.py
    |- static
        |- css
            |- innocent.min.css
            |- innocent.min.css.map
    |- templates
        |- app_name
            |- base.html
            |- home_page.html
            |- thing_delete.html
            |- thing_engaged.html
            |- thing_form.html
            |- thing_iterate.html
            |- thing_listed.html
            |- thing_optimize.html
            |- thing_page.html
|- schemaorg
    |- data
        |- releases
            |- 3.9
                |- all-layers.jsonld
```

### Seeing is Believing

Now you can follow the onscreen instructions to setup a `virtualenv`; install models; and run the website. Assuming you followed the Quick Start exactly (which will assume for the rest of this guide), you'll type the following.

```
source venv-frankenstein/bin/activate[.fish]
pip install -e .
./init_frankenstein.sh
```

Navigate to <http://localhost:8000>

You will now see a working app!

### What we learnt

- How to use `yo thing:django` to install an elioApp app.
