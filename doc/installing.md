# Installing generator-thing

- [generator-thing Prerequisites](/eliothing/generator-thing/prerequisites.html)

## Install

- Install eliosin's yeoman generator, **generator-thing** globally.

```shell
npm install -g yo
npm install -g generator-thing
```
