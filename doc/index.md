<aside>
  <dl>
  <dd>created all</dd>
  <dd>Such to perfection, one first matter all,</dd>
  <dd>Endued with various forms, various degrees</dd>
  <dd>Of substance, and, in things that live</dd>
</dl>
</aside>

**generator-thing** is a yeoman generator for building websites with **eliothing**'s models. With this platform you can rapidly create Django apps modelled on <https://schema.org> "Things".

You can use it like this:

```shell
npm install -g generator-thing
# scaffold root folder basics like gitignore, README, etc
yo thing
yo thing:repo
# produce a JSON file which is an instance of a Thing (thinglet).
yo thing:thing --identifier my-app-name --subjectOf elioSuperGroup
# Scaffold apps driven by libraries built for **the elioWay**
yo thing:django --identifier my-django-name --subjectOf elioSuperGroup
yo thing:flask --identifier my-flask-name --subjectOf elioSuperGroup
yo thing:node --identifier my-node-name --subjectOf elioSuperGroup
```
