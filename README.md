![](https://elioway.gitlab.io/eliothing/generator-thing/elio-generator-Thing-logo.png)

> Everything you need to get life started **the elioWay**

# generator-thing ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

**generator-thing** is a yeoman generator for Schema.org backed projects driven, **the elioWay**.

(So for install if your want an app called "Thing" created )

- Thou shalt multiple theyself, Thing, across many types, formats and contexts.

TODO Index of Commands

- [generator-thing Documentation](https://elioway.gitlab.io/eliothing/generator-thing)

## Installing

```shell
npm install -g generator-thing
yarn global add generator-thing
```

- [Installing generator-thing](https://elioway.gitlab.io/eliothing/generator-thing/installing.html)

## Seeing is Believing

```shell
npm install -g generator-thing
yo thing:thing --identifier my-app-name --subjectOf elioSuperGroup
```

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Nutshell

### `yo thing`

- `yo thing:repo` is the default.
- `yo thing:python`
- `yo thing:flask`
- `yo thing:django`
- `yo thing:node`

### `npm test`

### `npm test -- fileName`

### `npm run prettier`

### `ncu -u -t minor`

- [generator-thing Quickstart](https://elioway.gitlab.io/eliothing/generator-thing/quickstart.html)
- [generator-thing Credits](https://elioway.gitlab.io/eliothing/generator-thing/credits.html)

![](https://elioway.gitlab.io/eliothing/generator-thing/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
