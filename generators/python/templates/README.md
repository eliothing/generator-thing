![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/elio-<%= kebabName %>-logo.png)

> <%= CamelName %>, **the elioWay**

# <%= identifier %>

Starter pack for an eliothing python app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [<%= identifier %> Documentation](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/)

## Installing

```
pip install <%= kebabName %>
<%= kebabName %>
```

- [Installing <%= identifier %>](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/installing.html)

## Requirements

- [<%= subjectOf %> Prerequisites](https://elioway.gitlab.io/<%= subjectOf %>/installing.html)

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Nutshell

- [<%= subjectOf %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/quickstart.html)
- [<%= identifier %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/quickstart.html)

# Credits

- [<%= identifier %> Credits](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/apple-touch-icon.png)
