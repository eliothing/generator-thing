"use strict"
const Thingrator = require("../thingrator")
const eliosay = require("@elioway/say")

module.exports = class Pythonator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Pythonic Generation")
      this._blackquote(
        `So talked the spirited sly Snake; and Eve, \n` +
          `Yet more amazed, unwary thus replied.`,
      )
    }
    this.composeWith("thing:repo", {
      arguments: [this.identifier, this.subjectOf],
      composedBy: "Pythonator",
    })
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `Serpent, thy overpraising leaves in doubt\n` +
          `The virtue of that fruit, in thee first proved`,
      )
    }
  }
  writing() {
    this.paths()
    let snakeName = this.snakeName
    this._write("_env", ".env")
    this._write("_env.fish", ".env.fish")
    this._write("elio_snakeName.py", `elio_${snakeName}.py`)
    this._write("snakeName.py", `${snakeName}.py`)
    this._write("pyproject.toml")
    this._write("README.md")
    this._write("setup.cfg")
    this._write("setup.py")
    this._write("snakeName/__init__.py", `${snakeName}/__init__.py`)
    this._write("snakeName/snakeName.py", `${snakeName}/${snakeName}.py`)
    this._write(
      "snakeName/tests/test_snakeName.py",
      `${snakeName}/tests/test_${snakeName}.py`,
    )
    this._copy("requirements/base.txt")
    this._copy("requirements/local.txt")
  }
  install() {
    if (!this.options.skipinstall) {
      let snakeName = this.snakeName
      this.spawnCommandSync("virtualenv", [
        "--python=python3",
        `venv-${snakeName}`,
      ])
      this.spawnCommandSync("echo", [`\n\nRUN:`])
      this.spawnCommandSync("echo", [
        `source venv-${snakeName}/bin/activate`,
        `source venv-${snakeName}/bin/activate.fish`,
      ])
      this.spawnCommandSync("echo", ["pip install -r requirements/local.txt"])
    }
  }
}
