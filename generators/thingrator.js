"use strict"
const Generator = require("yeoman-generator")
const path = require("path")

var {
  camelCase,
  capitalize,
  kebabCase,
  snakeCase,
  upperFirst,
} = require("lodash")

module.exports = class Thingrator extends Generator {
  constructor(args, opts) {
    super(args, opts)
    this.argument("identifier", { type: String, required: false })
    this.argument("subjectOf", { type: String, required: false })
    this.option("skipinstall")
  }
  /** paths
   * @helper Set the "generate into here" folder. We've aimed to ensure that
   * a new folder is created if the folder name you are in does not
   * match your current one. */
  paths() {
    let subFolder =
      this.options.identifier === this.appname || !this.options.identifier
        ? ""
        : this.options.identifier
    this.destinationRoot(`${this.contextRoot}${path.sep}${subFolder}`)
  }
  /** install */
  install() {
    if (!this.options.skipinstall) {
      this.npmInstall()
    }
  }
  /** _getOrTemplate
   * @helper Fetches a file from the existing file set, or uses the local version instead.
   */
  _getOrTemplate(fileName) {
    var file = ""
    if (this.fs.exists(this.destinationPath(fileName))) {
      file = this.destinationPath(fileName)
    } else {
      file = this.templatePath(fileName)
    }
    return this.fs.read(file)
  }
  /** HELPER: General use templater for all kinds of templates. */
  _write(templateName, toFileName, extraParams) {
    // this.fs.delete(fileName)
    if (!toFileName) toFileName = templateName
    this.fs.copyTpl(
      this.templatePath(templateName),
      this.destinationPath(toFileName),
      {
        CamelName: this.CamelName,
        camelName: this.camelName,
        folderName: this.folderName,
        identifier: this.identifier,
        kebabName: this.kebabName,
        ProperName: this.ProperName,
        snakeName: this.snakeName,
        subjectOf: this.subjectOf,
        UPPERNAME: this.UPPERNAME,
        year: new Date(Date.now()).getFullYear(),
        ...extraParams,
      },
    )
  }
  /** HELPER: Straightforward copy with no wildcards. */
  _copy(fromFileName, toFileName) {
    toFileName = toFileName ? toFileName : fromFileName
    // this.fs.delete(fileName)
    this.fs.copy(
      this.templatePath(fromFileName),
      this.destinationPath(toFileName),
    )
  }
  /** HELPER: Turn prompt answers into a JSArray. */
  _arrayifyCommaSeparation(str) {
    return str.replace(/,/g, " ").trim().split(" ").filter(String)
  }
  /** HELPER: Speak to the people. */
  _eliosay(mess) {
    let top = `
   __________________________________________________________
 =(__    ___        _____      ___       _______      __    _)=
   |                                                        |
   |                  **Paradise Lost**                     |
   |                 Book VI, Lines 23-27${mess}                   |
   |                          ~                             |
   |                                                        |`
    this.log(top)
  }
  /** HELPER: Speak to the people. */
  _blackquote(mess) {
    let newLine = "\n"
    let bottom = `
    |                                                        |
    |                                                        | 
 KCK|__    ___   __    ___ _    ___   __   _    ___   __    _| gabriel, the elioWay                                  |
  =(__________________________________________________________)=
  
 
 `
    mess.split(newLine).forEach((line) => this.log("   | " + line))
    this.log(bottom)
  }
  get CamelName() {
    return upperFirst(camelCase(this.identifier))
  }
  get camelName() {
    return camelCase(this.identifier)
  }
  get subjectOf() {
    if (this.options.subjectOf) {
      return this.options.subjectOf
    } else {
      let pathTo = this.finalPath.split(path.sep)
      return pathTo[pathTo.length - 2]
    }
  }
  get finalPath() {
    let pathTo = this.contextRoot.split(path.sep)
    // identifier, but path not terminated by identifier.
    if (
      this.options.identifier &&
      pathTo[pathTo.length - 1] !== this.options.identifier
    ) {
      // Push identifier to end of path, otherwise we will use path terminator.
      pathTo.push(this.options.identifier)
    }
    // subjectOf, but identifier not child subjectOf in path
    if (
      this.options.subjectOf &&
      pathTo[pathTo.length - 2] !== this.options.subjectOf
    ) {
      // Insert subjectOf as parent of identifier dir.
      pathTo.splice(-1, 0, this.options.subjectOf)
    }
    return pathTo.join(path.sep)
  }
  get folderName() {
    return this.identifier
  }
  get kebabName() {
    return kebabCase(this.identifier)
  }
  get identifier() {
    let pathTo = this.finalPath.split(path.sep)
    return pathTo.pop()
  }
  get ProperName() {
    return capitalize(this.identifier)
  }
  get snakeName() {
    return snakeCase(this.identifier)
  }
  get UPPERNAME() {
    return this.CamelName.toUpperCase()
  }
}
