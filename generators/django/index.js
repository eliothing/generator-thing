"use strict"
const Pythonator = require("../python/index")

module.exports = class Djangator extends Pythonator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generating Django")
      this._blackquote(
        `                                the harp \n` +
          `Had work and rested not; the solemn pipe, \n` +
          `And dulcimer, all organs of sweet stop,`,
      )
    }
    this.composeWith("thing:python", {
      arguments: [this.identifier, this.subjectOf],
      composedBy: "Djangator",
    })
  }
  end() {
    if (!this.options.skipinstall) {
      this.spawnCommandSync("echo", [`./run_${this.snakeName}.sh`])
    }
    if (!this.options.composedBy) {
      this._blackquote(
        `All sounds on fret by string or golden wire, \n` +
          `Tempered soft tunings`,
      )
    }
  }
  writing() {
    this.paths()
    let snakeName = this.snakeName
    this._write(`manage.py`)
    this._write("README.md")
    this._write(`run_snakeName.sh`, `run_${snakeName}.sh`)
    this._write(`setup.py`)
    this._write(`snakeName/models.py`, `${snakeName}/models.py`)
    this._write(`snakeName/settings.py`, `${snakeName}/settings.py`)
    this._write(`snakeName/urls.py`, `${snakeName}/urls.py`)
    this._write(`snakeName/views.py`, `${snakeName}/views.py`)
    this._write(`snakeName/wsgi.py`, `${snakeName}/wsgi.py`)
    this._copy(
      "snakeName/static/manifest.json",
      `${snakeName}/static/manifest.json`,
    )
    this._copy(
      `snakeName/static/css/snakeName.min.css`,
      `${snakeName}/static/css/${snakeName}.min.css`,
    )
    this._copy(
      `snakeName/static/css/snakeName.min.css.map`,
      `${snakeName}/static/css/${snakeName}.min.css.map`,
    )
    this._copy(
      "snakeName/static/js/jquery-3.4.1.min.js",
      `${snakeName}/static/js/jquery-3.4.1.min.js`,
    )
    this._write(
      `snakeName/templates/snakeName/base.html`,
      `${snakeName}/templates/${snakeName}/base.html`,
    )
    this._write(
      `snakeName/templates/snakeName/home_page.html`,
      `${snakeName}/templates/${snakeName}/home_page.html`,
    )
    this._write(
      `snakeName/templates/snakeName/thing_delete.html`,
      `${snakeName}/templates/${snakeName}/thing_delete.html`,
    )
    this._write(
      `snakeName/templates/snakeName/thing_engaged.html`,
      `${snakeName}/templates/${snakeName}/thing_engaged.html`,
    )
    this._write(
      `snakeName/templates/snakeName/thing_form.html`,
      `${snakeName}/templates/${snakeName}/thing_form.html`,
    )
    this._write(
      `snakeName/templates/snakeName/thing_many.html`,
      `${snakeName}/templates/${snakeName}/thing_many.html`,
    )
    this._write(
      `snakeName/templates/snakeName/thing_listed.html`,
      `${snakeName}/templates/${snakeName}/thing_listed.html`,
    )
    this._write(
      `snakeName/templates/snakeName/thing_optimize.html`,
      `${snakeName}/templates/${snakeName}/thing_optimize.html`,
    )
    this._write(
      `snakeName/templates/snakeName/thing_page.html`,
      `${snakeName}/templates/${snakeName}/thing_page.html`,
    )
    this._copy(`requirements/base.txt`)
    this._copy(`requirements/local.txt`)
    this._copy(`schemaorg/data/releases/3.9/all-layers.jsonld`)
  }
}
