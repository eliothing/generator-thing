![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/elio-<%= kebabName %>-logo.png)

> <%= CamelName %>, **the elioWay**

# <%= identifier %>

Your new creature built **the elioWay**.

- [dna Documentation](https://elioway.gitlab.io/eliothing/dna)

## Seeing is Believing

```shell
virtualenv --python=python3 venv-<%= snakeName %>
source venv-<%= snakeName %>/bin/activate.fish
pip install -r requirements/local.txt
./run_<%= snakeName %>.sh
```

## genome Management command

To help you find the right schema Thing, use our genome explorer: `django-admin genome <class/property>`

```shell
django-admin genome Thing
django-admin genome Person
django-admin genome isbn
django-admin genome description
django-admin genome image
```

## Nutshell

```
django-admin runserver 0.0.0.0:8000
```

- [<%= subjectOf %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/quickstart.html)
- [<%= identifier %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/quickstart.html)

# Credits

- [<%= identifier %> Credits](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/credits.html)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)

![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/apple-touch-icon.png)
