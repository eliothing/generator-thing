#!/bin/bash
# treat unset variables as an error when substituting.
set -u
# exit immediately if a command exits with a nonzero exit status.
set -e

export DJANGO_SETTINGS_MODULE="<%= snakeName %>.settings"

touch <%= snakeName %>.db # && rm <%= snakeName %>.db
django-admin makemigrations <%= snakeName %>
django-admin migrate --noinput --database=default
django-admin collectstatic --noinput
django-admin runserver 0.0.0.0:8000
