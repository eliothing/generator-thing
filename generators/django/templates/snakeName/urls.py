# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from <%= snakeName %>.views import (
    <%= CamelName %>CreateView,
    <%= CamelName %>UpdateView,
    <%= CamelName %>DeleteView,
    <%= CamelName %>EngageView,
    <%= CamelName %>ListView,
    <%= CamelName %>ManyView,
    <%= CamelName %>OptimizeView,
    <%= CamelName %>PageView,
    <%= CamelName %>HomeView,
)

urlpatterns = [
    path(
        "cr/<str:page_slug>/<str:role>/<str:thing>",
        <%= CamelName %>CreateView.as_view(),
        name="create.thing",
    ),
    path(
        "u/<str:page_slug>/<str:role>/<str:thing>/<int:pk>",
        <%= CamelName %>UpdateView.as_view(),
        name="update.thing",
    ),
    path(
        "d/<str:page_slug>/<str:thing>/<int:pk>",
        <%= CamelName %>DeleteView.as_view(),
        name="delete.thing",
    ),
    path(
        "e/<str:page_slug>/<str:thing>/<int:pk>",
        <%= CamelName %>EngageView.as_view(),
        name="engage.thing",
    ),
    path(
        "l/<str:page_slug>/<str:thing>",
        <%= CamelName %>ListView.as_view(),
        name="list.things",
    ),
    path(
        "m/<str:page_slug>/<str:thing>/<int:pk>/<str:prop>",
        <%= CamelName %>ManyView.as_view(),
        name="many.things",
    ),
    path(
        "o/<str:page_slug>/<str:thing>",
        <%= CamelName %>OptimizeView.as_view(),
        name="optimize.things",
    ),
    path("p/<str:page_slug>", <%= CamelName %>PageView.as_view(), name="engage.page"),
    path("", <%= CamelName %>HomeView.as_view(), name="home"),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
