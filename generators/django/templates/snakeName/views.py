# -*- encoding: utf-8 -*-
from dna.views import (
    EveryCreateViewMixin,
    EveryUpdateViewMixin,
    EveryDeleteViewMixin,
    EveryEngageViewMixin,
    EveryListViewMixin,
    EveryManyViewMixin,
    EveryOptimizeViewMixin,
    EveryPageViewMixin,
    EveryHomeViewMixin,
)


class <%= CamelName %>CreateView(EveryCreateViewMixin):
    template_name = "<%= snakeName %>/thing_form.html"


class <%= CamelName %>UpdateView(EveryUpdateViewMixin):
    template_name = "<%= snakeName %>/thing_form.html"


class <%= CamelName %>DeleteView(EveryDeleteViewMixin):
    template_name = "<%= snakeName %>/thing_delete.html"


class <%= CamelName %>EngageView(EveryEngageViewMixin):
    template_name = "<%= snakeName %>/thing_engaged.html"


class <%= CamelName %>ListView(EveryListViewMixin):
    template_name = "<%= snakeName %>/thing_listed.html"


class <%= CamelName %>ManyView(EveryManyViewMixin):
    template_name = "<%= snakeName %>/thing_many.html"


class <%= CamelName %>OptimizeView(EveryOptimizeViewMixin):
    template_name = "<%= snakeName %>/thing_optimize.html"


class <%= CamelName %>PageView(EveryPageViewMixin):
    template_name = "<%= snakeName %>/thing_page.html"


class <%= CamelName %>HomeView(EveryHomeViewMixin):
    template_name = "<%= snakeName %>/home_page.html"
