![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/elio-<%= kebabName %>-logo.png)

> <%= CamelName %>, **the elioWay**

# <%= identifier %>

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [<%= identifier %> Documentation](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/)

## Prerequisites

- [<%= identifier %> Prerequisites](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/installing.html)

## Installing

- [Installing <%= identifier %>](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [<%= subjectOf %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/quickstart.html)
- [<%= identifier %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/quickstart.html)

# Credits

- [<%= identifier %> Credits](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/apple-touch-icon.png)
