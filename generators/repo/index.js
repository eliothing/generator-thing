"use strict"
const Thingrator = require("../thingrator")

module.exports = class Reporator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generation GIT")
      this._blackquote(
        `             none whose portion is so small \n` +
          `Of present pain that with ambitious mind \n` +
          `Will covet more!`,
      )
    }
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `                  With this advantage, then, \n` +
          `To union, and firm faith, and firm accord, \n` +
          `More than can be in Heaven, we now return \n` +
          `To claim our just inheritance`,
      )
    }
  }
  writing() {
    this.paths()
    this._copy("_gitignore", ".gitignore")
    this._copy("_npmignore", ".npmignore")
    this._copy("_prettierignore", ".prettierignore")
    this._copy("_prettierrc", ".prettierrc")
    this._write("LICENSE")
    this._write("package.json")
    this._write("README.md")
  }
}
