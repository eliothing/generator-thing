import { should } from "chai"
import <%= camelName %> from "../<%= kebabName %>"

should()

describe("module | <%= camelName %>", (hooks) => {
  it("fetches <%= camelName %>", () => {
    <%= camelName %>().should.equal("<%= kebabName %>")
  })
})
