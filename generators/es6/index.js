"use strict"
const Noderator = require("../node")

module.exports = class Esfiverator extends Noderator {
  writing() {
    this.paths()
    const kebabName = this.kebabName
    // Update the package.json.
    const pkgJson = {
      name: `@elioway/${kebabName}`,
      bin: "./bin/index.js",
      main: `./${kebabName}.js`,
      private: true,
      devDependencies: {
        chai: "^4.3.7",
        mocha: "^10.1.0",
        prettier: "2.8.0",
      },
      scripts: {
        [kebabName]: `node ./${kebabName}.js`,
        test: "mocha",
      },
      type: "module",
    }
    // this._write("package.json") replaced
    this.fs.extendJSON(this.destinationPath("package.json"), pkgJson)
    // Copy and write
    this._write("kebabName.js", `${kebabName}.js`)
    this._write("bin/index.js")
    this._copy("test/always-passes-test.mjs")
    this._copy("test/freeze-date-test.mjs")
    this._write("test/kebabName-test.mjs", `test/${kebabName}-test.mjs`)
  }
}
