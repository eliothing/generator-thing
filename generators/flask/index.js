"use strict"
const Thingrator = require("../thingrator")

module.exports = class Flaskator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generating Flask")
      this._blackquote(
        `                       and now with ease, \n` +
          `Wafts on the calmer wave by dubious light,`,
      )
    }
    this.composeWith("thing:python", {
      arguments: [this.identifier, this.subjectOf],
      composedBy: "Flaskator",
    })
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `like a weather-beaten vessel, holds \n` +
          `Gladly the port, though shrouds and tackle torn; \n` +
          `Or in the emptier waste, resembling air`,
      )
    }
  }
  writing() {
    this.paths()
    let snakeName = this.snakeName
    this._write("README.md")
    this._write("run.py")
    this._write("setup.py")
    this._write("wsgi.py")
    this._write("doc/installing.md")
    this._write("installers/snakeName", `installers/${snakeName}`)
    this._write(
      "installers/snakeName.service",
      `installers/${snakeName}.service`,
    )
    this._write("snakeName/app.py", `${snakeName}/app.py`)
    this._write("snakeName/config.py", `${snakeName}/config.py`)
    this._write("snakeName/jinja_filters.py", `${snakeName}/jinja_filters.py`)
    this._write("snakeName/utils.py", `${snakeName}/utils.py`)
    this._write("snakeName/views.py", `${snakeName}/views.py`)
    this._copy(
      "snakeName/static/manifest.json",
      `${snakeName}/static/manifest.json`,
    )
    this._copy(
      "snakeName/static/css/snakeName.min.css",
      `${snakeName}/static/css/${snakeName}.min.css`,
    )
    this._copy(
      "snakeName/static/css/snakeName.min.css.map",
      `${snakeName}/static/css/${snakeName}.min.css.map`,
    )
    this._write(
      `snakeName/templates/base.html`,
      `${snakeName}/templates/base.html`,
    )
    this._write(
      `snakeName/templates/index.html`,
      `${snakeName}/templates/index.html`,
    )
    this._write(
      `snakeName/templates/snakeName.html`,
      `${snakeName}/templates/${snakeName}.html`,
    )
    this._copy("requirements/base.txt")
    this._copy("requirements/local.txt")
  }
}
