![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/elio-<%= kebabName %>-logo.png)

> <%= CamelName %>, **the elioWay**

# <%= identifier %>

A Flask app built **the elioWay**.

- [<%= identifier %> Documentation](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/)

  ## Seeing is Believing

```shell
virtualenv --python=python3 venv-<%= snakeName %>
source venv-<%= snakeName %>/bin/activate
# or
source venv-<%= snakeName %>/bin/activate.fish
pip install -r requirements/local.txt
./run.py
```

- USER: <%= snakeName %>
- PASS: letmein

Navigate to [<%= identifier %>](http://localhost:5000)

## Nutshell

```
./run.py
```

- [<%= subjectOf %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/quickstart.html)
- [<%= identifier %> Quickstart](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/quickstart.html)

# Credits

- [<%= identifier %> Credits](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/credits.html)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)

![](https://elioway.gitlab.io/<%= subjectOf %>/<%= identifier %>/apple-touch-icon.png)
