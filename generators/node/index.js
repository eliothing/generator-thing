"use strict"
const Thingrator = require("../thingrator")

module.exports = class Noderator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generating NodeJs")
      this._blackquote(
        `                   glory, whom their hate \n` +
          `Illustrates, when they see all regal power \n` +
          `Given me to quell their pride, `,
      )
    }
    this.composeWith("thing:repo", {
      arguments: [this.identifier, this.subjectOf],
      composedBy: "Noderator",
    })
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `                          and in event \n` +
          `Know whether I be dextrous to subdue \n` +
          `Thy rebels, or be found the worst in Heaven`,
      )
    }
  }
  writing() {
    this.paths()
    const kebabName = this.kebabName
    // Update the package.json.
    const pkgJson = {
      name: `@elioway/${kebabName}`,
      bin: "./bin/index.js",
      main: `./${kebabName}.js`,
      devDependencies: {
        chai: "^4.3.7",
        mocha: "^10.1.0",
      },
      scripts: {
        [kebabName]: `node ./${kebabName}.js`,
        test: "mocha",
      },
    }
    // this._write("package.json") replaced
    this.fs.extendJSON(this.destinationPath("package.json"), pkgJson)
    // Copy and write
    this._write("kebabName.js", `${kebabName}.js`)
    this._write("bin/index.js")
    this._copy("test/always-passes-test.mjs")
    this._copy("test/freeze-date-test.mjs")
    this._write("test/kebabName-test.mjs", `test/${kebabName}-test.mjs`)
  }
}
