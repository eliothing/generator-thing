const should = require("chai").should()
const <%= camelName %> = require("../<%= kebabName %>")

describe("module | <%= camelName %>", function (hooks) {
  it("fetches <%= camelName %>", () => {
    <%= camelName %>().should.equal("<%= kebabName %>")
  })
})
