"use strict"
const Thingrator = require("../thingrator")
const ThingBuilder = require("@elioway/thing/thing-builder")
const { schemaDomainUrl } = require("@elioway/thing/utils/get-schema")

module.exports = class Reporator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generating things")
      this._blackquote(
        `                          where each \n` +
          `To other speedy aid might lend at need`,
      )
    }
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `And render me more equal; and perhaps, \n` + `A thing not undesirable`,
      )
    }
  }
  prompting() {
    const prompts = [
      {
        type: "input",
        name: "mainEntityOfPage",
        message: "What Schema Type do you want to create?",
        default: this.config.get("Thing") || "Thing",
      },
      {
        type: "input",
        name: "scheme",
        message: "Output the schemas?",
        default: this.config.get("scheme") || false,
      },
    ]
    return this.prompt(prompts).then((answer) => {
      this.answer = answer
    })
  }
  writing() {
    this.paths()
    // Build a Thing
    let thingBuilder = new ThingBuilder(
      "schemaorg/data/releases/9.0/schemaorg-all-http",
      schemaDomainUrl,
    )
    // Build
    let Thing = thingBuilder.Thing([this.answer.mainEntityOfPage])
    let thinglet = thingBuilder.thinglet(
      Thing[this.answer.mainEntityOfPage],
      this.answer.mainEntityOfPage,
    )
    thinglet.additionalType = this.CamelName
    thinglet.identifier = this.identifier
    thinglet.image = "star.png"
    thinglet.mainEntityOfPage =
      this.answer.mainEntityOfPage !== "Thing"
        ? this.answer.mainEntityOfPage
        : "ItemList"
    thinglet.name = this.identifier
    thinglet.potentialAction = {
      mainEntityOfPage: "Action",
      identifier: "inflateT",
    }
    thinglet.subjectOf = this.subjectOf
    thinglet.url = `http://github.com/${this.identifier}`
    // Write Out
    this.fs.writeJSON(this.destinationPath("thing.json"), thinglet)
    if (this.answer.scheme) {
      this.fs.writeJSON(
        this.destinationPath("Schema/Thing.json"),
        Thing[this.answer.mainEntityOfPage],
      )
    }
  }
}
