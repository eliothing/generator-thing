"use strict"
const Thingrator = require("../thingrator")

module.exports = class Apparator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generating Things")
      this._blackquote(
        `                  needs must the Power \n` +
          `That made us, and for us this ample world, \n` +
          `Be infinitely good,`,
      )
    }
    this.composeWith("thing:repo", {
      arguments: [this.identifier, this.subjectOf],
      composedBy: Apparator,
    })
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `               and of his good \n` +
          `As liberal and free as infinite; \n` +
          `That raised us from the dust, and placed us here \n` +
          `In all this happiness`,
      )
    }
  }
}
